# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions

activate :directory_indexes

activate :asciidoc, base_dir: :source

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false
page '/*.svg', layout: false

page '/docs/*', layout: 'article'

set :build_dir, 'public'